package com.zhj.myapplication.music;

import androidx.fragment.app.Fragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.zhj.myapplication.R;
import com.zhj.myapplication.service.Service1;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BindMusicFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BindMusicFragment extends Fragment {
    private View view;
    private LinearLayout tab01,tab02;
    Service1.Mybinder binder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d("service1","bbbbbbbbbbbbbbb");
        view = inflater.inflate(R.layout.fragment_bind_music,container,false);



        Intent intentservices = new Intent(getActivity(),Service1.class);

        ServiceConnection connection= new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                binder = (Service1.Mybinder)iBinder;
                binder.myplay();
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {
                binder=null;
            }
        };


        tab01 = view.findViewById(R.id.bindtab01);
        tab02 = view.findViewById(R.id.bindtab02);

        tab01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("service1","bind start music....");
                getActivity().bindService(intentservices,connection, Context.BIND_AUTO_CREATE);//绑定服务
            }
        });

        tab02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("service1","bind stop music....");
                getActivity().unbindService(connection);
            }
        });

        return view;
    }
}
