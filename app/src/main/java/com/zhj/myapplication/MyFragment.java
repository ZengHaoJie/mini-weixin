package com.zhj.myapplication;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

/**
 * Description:
 */
public class MyFragment extends Fragment {
    private View view;
    private LinearLayout unbindmusic, bindmusic;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_my, container, false);

        unbindmusic = view.findViewById(R.id.unbindMusic);
        bindmusic = view.findViewById(R.id.bindMusic);

        unbindmusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity main = (MainActivity) getActivity();
                main.setSelect(5);

            }
        });

        bindmusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity main = (MainActivity) getActivity();
                main.setSelect(6);
            }
        });

        return view;
    }

}
