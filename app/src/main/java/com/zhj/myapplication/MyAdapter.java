package com.zhj.myapplication;

import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zhj.myapplication.entity.Fruit;
import com.zhj.myapplication.fruitActivity.AppleActivity;
import com.zhj.myapplication.fruitActivity.BananaActivity;
import com.zhj.myapplication.fruitActivity.OrangeActivity;
import com.zhj.myapplication.fruitActivity.PearActivity;

import java.util.List;


/**
 * Description:
 */
public class MyAdapter extends RecyclerView.Adapter {
    private List<Fruit> mFruitList;

    public MyAdapter(List<Fruit> mFruitList) {
        this.mFruitList = mFruitList;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        View fruitView;
        ImageView fruitImg;
        TextView fruitName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            fruitView = itemView;
            fruitImg = itemView.findViewById(R.id.fruit_img);
            fruitName = itemView.findViewById(R.id.fruit_name);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fruit_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        holder.fruitView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = holder.getAdapterPosition();
                Fruit fruit = mFruitList.get(position);
                holder.itemView.setBackgroundColor(Color.parseColor("#f4c542"));
                Toast.makeText(view.getContext(), "you clicked " + fruit.getName() + " view", Toast.LENGTH_LONG).show();
                if (fruit.getName().equals("Apple")) {
                    Intent intent = new Intent(view.getContext(), AppleActivity.class);
                    view.getContext().startActivity(intent);
                } else if (fruit.getName().equals("Banana")) {
                    Intent intent = new Intent(view.getContext(), BananaActivity.class);
                    view.getContext().startActivity(intent);
                } else if(fruit.getName().equals("Pear")) {
                    Intent intent = new Intent(view.getContext(), PearActivity.class);
                    view.getContext().startActivity(intent);
                } else if (fruit.getName().equals("Orange")) {
                    Intent intent = new Intent(view.getContext(), OrangeActivity.class);
                    view.getContext().startActivity(intent);
                } else {
                    Toast.makeText(view.getContext(), "you clicked view invalid", Toast.LENGTH_LONG).show();
                }
            }
        });
        holder.fruitImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = holder.getAdapterPosition();
                Fruit fruit = mFruitList.get(position);
                holder.itemView.setBackgroundColor(Color.parseColor("#7EC0EE"));
                Toast.makeText(view.getContext(), "I clicked " + fruit.getName() + " image", Toast.LENGTH_LONG).show();
                if (fruit.getName().equals("Apple")) {
                    Intent intent = new Intent(view.getContext(), AppleActivity.class);
                    view.getContext().startActivity(intent);
                } else if (fruit.getName().equals("Banana")) {
                    Intent intent = new Intent(view.getContext(), BananaActivity.class);
                    view.getContext().startActivity(intent);
                } else if(fruit.getName().equals("Pear")) {
                    Intent intent = new Intent(view.getContext(), PearActivity.class);
                    view.getContext().startActivity(intent);
                } else if (fruit.getName().equals("Orange")) {
                    Intent intent = new Intent(view.getContext(), OrangeActivity.class);
                    view.getContext().startActivity(intent);
                } else {
                    Toast.makeText(view.getContext(), "you clicked image invalid", Toast.LENGTH_LONG).show();
                }
            }
        });
        return holder;
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder reholder = (ViewHolder) holder;
        Fruit fruit = mFruitList.get(position);
        reholder.fruitImg.setImageResource(fruit.getImgId());
        reholder.fruitName.setText(fruit.getName());

    }

    @Override
    public int getItemCount() {
        return mFruitList.size();
    }
}
