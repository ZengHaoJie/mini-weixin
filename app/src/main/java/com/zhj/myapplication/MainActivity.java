package com.zhj.myapplication;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.zhj.myapplication.entity.Fruit;
import com.zhj.myapplication.music.BindMusicFragment;
import com.zhj.myapplication.music.UnbindMusicFragment;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private final Fragment fragment_weixin = new WeixinFragment();
    private final Fragment fragment_friend = new FriendsFragment();
    private final Fragment fragment_find = new FindFragment();
    private final Fragment fragment_my = new MyFragment();
    private final Fragment bindmusicfragment = new BindMusicFragment();
    private final Fragment unbindmusicfragment = new UnbindMusicFragment();

    private FragmentManager fm;
    private FragmentTransaction transaction;

    private LinearLayout tabWeixin;
    private LinearLayout tabFriend;
    private LinearLayout tabFind;
    private LinearLayout tabMy;

    private ImageButton imgWeixin;
    private ImageButton imgFriend;
    private ImageButton imgFind;
    private ImageButton imgMy;

    //动态授权
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d("aaa","??????");
        switch (requestCode){
            case 1:
                if(grantResults[0]!= PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(this, "未授权，无法实现预定的功能！", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(this, "请发一条短信验证！", Toast.LENGTH_SHORT).show();
                }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.mainlayout);

        Log.d("activity","11111111111");

        //授权
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{"android.permission.RECEIVE_SMS"}, 1);
        }

        // 去掉它自带的标题框
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        initView(); // 调用关联对象函数
        initFragment(); // 调用添加界面函数
        transaction = fm.beginTransaction();
        hidefragment(transaction);

        // 监听
        tabWeixin.setOnClickListener(this);
        tabFriend.setOnClickListener(this);
        tabFind.setOnClickListener(this);
        tabMy.setOnClickListener(this);


    }

    // 初始化Fragment
    private void initFragment() {
        fm = getSupportFragmentManager();
        transaction = fm.beginTransaction()
                .add(R.id.content, fragment_weixin)
                .add(R.id.content, fragment_friend)
                .add(R.id.content, fragment_find)
                .add(R.id.content, fragment_my)
                .add(R.id.content, bindmusicfragment)
                .add(R.id.content, unbindmusicfragment);
        transaction.commit();
    }

    // 找到控件
    private void initView() {
        tabWeixin = findViewById(R.id.LinearLayout_weixin);
        tabFriend = findViewById(R.id.LinearLayout_friends);
        tabFind = findViewById(R.id.LinearLayout_find);
        tabMy = findViewById(R.id.LinearLayout_my);

        imgWeixin = findViewById(R.id.imgView_weixin);
        imgFriend = findViewById(R.id.imgView_friends);
        imgFind = findViewById(R.id.imgView_find);
        imgMy = findViewById(R.id.imgView_my);
    }

    // 换图片
    public void setSelect(int i) {
        FragmentTransaction transaction = fm.beginTransaction();
        hidefragment(transaction);
        switch (i) {
            case 1:
                transaction.show(fragment_weixin);
                imgWeixin.setImageResource(R.drawable.weixin_press);
                break;
            case 2:
                transaction.show(fragment_friend);
                imgFriend.setImageResource(R.drawable.tongxunlu_press);
                break;
            case 3:
                transaction.show(fragment_find);
                imgFind.setImageResource(R.drawable.faxian_press);
                break;
            case 4:
                transaction.show(fragment_my);
                imgMy.setImageResource(R.drawable.wode_press);
                break;
            case 5:
                transaction.show(unbindmusicfragment);
                break;
            case 6:
                transaction.show(bindmusicfragment);
                break;
            default:
                break;
        }
    }

    // 隐藏所有fragment
    private void hidefragment(@NonNull FragmentTransaction transaction) {

        transaction.hide(fragment_weixin);
        transaction.hide(fragment_friend);
        transaction.hide(fragment_find);
        transaction.hide(fragment_my);
        transaction.hide(bindmusicfragment);
        transaction.hide(unbindmusicfragment);
        transaction.commit();
    }

    // 重写onClick
    public void onClick(@NonNull View v) {
        resetimg();
        switch (v.getId()) {
            case R.id.LinearLayout_weixin:
                setSelect(1);
                break;
            case R.id.LinearLayout_friends:
                setSelect(2);
                break;
            case R.id.LinearLayout_find:
                setSelect(3);
                break;
            case R.id.LinearLayout_my:
                setSelect(4);
                break;
            default:
                break;
        }
    }

    //将未点击的图片按钮还原成原来的
    private void resetimg() {
        imgWeixin.setImageResource(R.drawable.weixin);
        imgFriend.setImageResource(R.drawable.tongxunlu);
        imgFind.setImageResource(R.drawable.faxian);
        imgMy.setImageResource(R.drawable.wode);
    }

}