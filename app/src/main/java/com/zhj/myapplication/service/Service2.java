package com.zhj.myapplication.service;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import androidx.annotation.Nullable;
import com.zhj.myapplication.R;


public class Service2 extends Service {

    MediaPlayer mediaPlayer;

    public class Mybinder extends Binder{
        public void myplay(){
            mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.shengerpingfan);
            mediaPlayer.start();
        }
    }

    //绑定非绑定都会启动此方法
    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("service1","service1 is oncreate");
        mediaPlayer=MediaPlayer.create(getApplicationContext(), R.raw.waiyuge);

    }

    //邦定式服务不启动此方法
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("service1","service1 is onStartCommand");
        mediaPlayer=MediaPlayer.create(this,R.raw.shengerpingfan);
        mediaPlayer.start();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Log.d("service1","service1 is onDestroy");
        mediaPlayer.stop();
        super.onDestroy();
    }



    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new Mybinder();


    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }


    @Override
    public boolean stopService(Intent name) {
        Log.d("service1","service1 is stop");
        return super.stopService(name);
    }
}
