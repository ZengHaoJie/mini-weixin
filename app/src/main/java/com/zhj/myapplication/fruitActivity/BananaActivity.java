package com.zhj.myapplication.fruitActivity;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.zhj.myapplication.R;

/**
 * Description:
 */
public class BananaActivity extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.banana_layout);
    }

}
