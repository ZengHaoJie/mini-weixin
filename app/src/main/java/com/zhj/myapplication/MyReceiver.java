package com.zhj.myapplication;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.zhj.myapplication.service.Service1;

public class MyReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        Log.d("onReceiver","get receiver");
        Intent receiverintent1 = new Intent(context, Service1.class);
        context.startService(receiverintent1);
        Intent receiverintent2 = new Intent(context,MainActivity.class);
        receiverintent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(receiverintent2);
    }
}
